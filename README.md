# Augmented Reality - Cinema 4D Project

## Deliverables of the Project

• 10 minute presentation 
• Including: 60 second rendered movie

## What we were asked to do

• Blend of video and computer graphics 
• In some scenes, the camera must move 
• Some graphics must be 3D 
• Most graphics must be registered to real objects/locations

## What I did

• Blend of video and computer graphics
• The camera moves in almost all the scenes. It doesn't move in only 20 frames.
• All graphics are 3D
• All graphics are registered to real objects/locations.